--Nurul Hidayah (Be-JS3)
--CREATE DATABASE
CREATE DATABASE game;

--akses DATABASE
\c game;

--MEMBUAT TABEL user_game
CREATE TABLE user_game (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

--MEMBUAT TABEL user_history
CREATE TABLE user_history (
    id BIGSERIAL PRIMARY KEY,
    user_game_id BIGINT NOT NULL,
    durasi VARCHAR(255) NOT NULL,
    skor BIGINT NOT NULL,
    waktu_permainan VARCHAR(255) NOT NULL
);

--MEMBUAT TABEL user_biodata
CREATE TABLE user_biodata (
    id BIGSERIAL PRIMARY KEY,
    user_game_id BIGINT NOT NULL,
    nama_lengkap VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    negara VARCHAR(255) NOT NULL
);

--MELIHAT DAFTAR TABEL
\dt

--MELIHAT DETAIL TABEL
\d+ user_game
\d+ user_history
\d+ user_biodata

--CREATE DATA
INSERT INTO user_game (username, password) VALUES ('jennierj', 'password');
INSERT INTO user_game (username, password) VALUES ('irene', 'password');
INSERT INTO user_game (username, password) VALUES ('conan11', 'password');

INSERT INTO user_history (user_game_id, durasi, skor, waktu_permainan) VALUES (1, '1 Jam 45 menit', '9090', '25-03-2021');
INSERT INTO user_history (user_game_id, durasi, skor, waktu_permainan) VALUES (2, '1 Jam 10 menit', '8900', '25-03-2021');
INSERT INTO user_history (user_game_id, durasi, skor, waktu_permainan) VALUES (3, '2 Jam 15 menit', '9000', '25-03-2021');
INSERT INTO user_history (user_game_id, durasi, skor, waktu_permainan) VALUES (1, '1 Jam 25 menit', '9990', '25-03-2021');
INSERT INTO user_history (user_game_id, durasi, skor, waktu_permainan) VALUES (1, '2 Jam 15 menit', '9790', '26-03-2021');
INSERT INTO user_history (user_game_id, durasi, skor, waktu_permainan) VALUES (2, '1 Jam 35 menit', '7900', '26-03-2021');

INSERT INTO user_biodata (user_game_id, nama_lengkap, email, negara) VALUES (1, 'jennie ruby jane', 'jennierj@gmail.com', 'California');
INSERT INTO user_biodata (user_game_id, nama_lengkap, email, negara) VALUES (2, 'irene adler', 'ireneadler@gmail.com', 'New York');
INSERT INTO user_biodata (user_game_id, nama_lengkap, email, negara) VALUES (3, 'conan gray', 'conanganteng123@gmail.com', 'Indonesia');

--READ DATA
SELECT * FROM user_game;
SELECT * FROM user_biodata;
SELECT * FROM user_history;

--Menampilkan user_game_id, username, nama lengkap, email, dan negara
SELECT user_game.id, 
    user_game.username, 
    user_biodata.nama_lengkap, 
    user_biodata.email, 
    user_biodata.negara 
FROM user_game
JOIN user_biodata on user_biodata.user_game_id = user_game.id;

--Menampilkan user_game.id, username, asal negara, waktu permainan, durasi, dan skor;
SELECT user_game.id, 
    user_game.username, 
    user_biodata.negara as asal_negara, 
    user_history.waktu_permainan, 
    user_history.durasi, 
    user_history.skor
FROM user_game
JOIN user_biodata on user_biodata.user_game_id = user_game.id
JOIN user_history on user_history.user_game_id = user_game.id;

--Menampilkan user_game_id, username, durasi, skor dan waktu permainan yang bermain pasa 26-03-2021
SELECT user_history.user_game_id,
    user_game.username,
    user_history.durasi,
    user_history.skor,
    user_history.waktu_permainan
FROM user_game
JOIN user_history on user_history.user_game_id = user_game.id
WHERE user_history.waktu_permainan = '26-03-2021';

--Menampilkan data user_game_id, username, waktu_permainan, dan skor yang lebih dari sama dengan 9000
SELECT user_history.user_game_id,
    user_game.username,
    user_history.waktu_permainan,
    user_history.skor
FROM user_game
JOIN user_history ON user_history.user_game_id = user_game.id
where user_history.skor >= '9000';

--Menampilkan data user dan jumlah permainan
SELECT user_game.id,
    user_game.username,
    user_biodata.email,
    user_biodata.negara,
    count(user_history.user_game_id) as jumlah_permainan
FROM user_game
JOIN user_biodata ON user_biodata.user_game_id = user_game.id
JOIN user_history ON user_history.user_game_id = user_game.id
GROUP BY user_game.id, 
    user_game.username,
    user_biodata.email,
    user_biodata.negara;

--Menampilkan id, username, dan jumlah permainan berdasarkan jumlah permainan tertinggi
SELECT user_game.id,
    user_game.username,
    count(user_history.user_game_id) as jumlah_permainan
FROM user_game
JOIN user_history ON user_history.user_game_id = user_game.id
GROUP BY user_game.id, 
    user_game.username
ORDER BY jumlah_permainan desc;

--UPDATE DATA
--Mengubah username 
UPDATE user_game
SET username = 'ireneAdl'
WHERE id = 2;

--Mengubah email
UPDATE user_biodata
set email = 'conangantenk76@gmail.com'
WHERE user_game_id = 3;

--Mengubah skor permainan, waktu permainan, dan durasi
UPDATE user_history
SET skor = 9375,
    waktu_permainan = '26-03-2021',
    durasi = '2 Jam 45 menit'
WHERE user_game_id = 3;


--DELETE DATA
--menghapus data history game yang skornya < 8000
DELETE FROM user_history
WHERE skor < 8000;