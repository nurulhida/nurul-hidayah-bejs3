module.exports = (req,res,next) => {
    res.respond = (
        data = null, 
        message = '', 
        status = true, 
        statuscode = 200
        ) => {

        if (statuscode >= 400) {
            status=false
        }

        res.status(statuscode).json({
            status,
            message,
            data
        });
    };

    
    res.respondBadReq = (message = 'bad request', data = null) => {
        res.respond(data, message, 400);
    };


    next();
}