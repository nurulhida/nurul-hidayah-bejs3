const express = require('express');
const router = express.Router();

const user_game = require('./controllers/userGameController');
const user_biodata = require('./controllers/userBiodataController');
const user_history = require('./controllers/userHistoryController');

const auth = require('./controllers/authController')
const midAuth = require('./middlewares/midAuth')
const ImageKit = require('./middlewares/index');
//home
router.get('/', (req,res) => {
    res.status(200).json({
        status: true,
        message: 'selamat datang di challenge chapter7 oleh Nurul Hidayah',
        data: null
    })
})

router.get('/user_game', user_game.getAll);
router.get('/user_game/:id', user_game.getDetail);
router.post('/user_game', user_game.createUgame);
router.put('/user_game/:id',  user_game.updateUgame);
router.delete('/user_game/:id', user_game.deleteUgame);

router.get('/user_biodata',  user_biodata.getAll);
router.get('/user_biodata/:id',  user_biodata.getDetail);
router.post('/user_biodata', user_biodata.createUbio);
router.put('/user_biodata/:id',  user_biodata.updateUbio);
router.delete('/user_biodata/:id', user_biodata.deleteUbio);

router.get('/user_history', user_history.getAll);
router.get('/user_history/:id', user_history.getDetail);
router.post('/user_history',  user_history.createUhis);
router.put('/user_history/:id', user_history.updateUhis);
router.delete('/user_history/:id',user_history.deleteUhis);

//upload file
const imageStorage = require('./middlewares/imageStorage');
const imageController = require('./controllers/imageController');
const videoStorage = require('./middlewares/videoStorage');
const videoController = require('./controllers/videoController');
const fileStorage = require('./middlewares/fileStorage');
const fileController = require('./controllers/fileController');
const { imagekit } = require('./helpers/kit');

//registrasi admin
router.post('/regist', auth.register)

//authentikasi untuk mendapat token untuk mengupload gambar/video/file
router.post('/auth/login-admin', auth.login)

//upload gambar
router.post('/upload/image', midAuth.login, midAuth.admin, imageStorage.single('image'), imageController.single);
router.post('upload/imagekit', ImageKit, imageController.imagekit)
router.post('/upload/video', midAuth.login, videoStorage.single('video', 5), videoController.multi);
router.post('/upload/file', midAuth.login, fileStorage.single('file', 15), fileController.multi)




module.exports = router;