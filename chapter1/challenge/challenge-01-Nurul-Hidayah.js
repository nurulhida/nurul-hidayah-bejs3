const readline = require('readline');

const koneksi = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


function akar(v) {
    return hasil = Math.sqrt(v);
}

function kuadrat(w) {
    return hasil = w ** 2;
}

function tambah(a, b) {
    return parseInt(a) + parseInt(b);
}

function kurang (c, d) {
    return parseInt(c) - parseInt(d);
}

function kali(e, f) {
    return parseInt(e) * parseInt(f);
}

function bagi(g, h) {
    return parseInt(g) / parseInt(h);
}

function luas_persegi(s) {
    return s*s;
}

function volume_kubus(s1) {
    return s1*s1*s1;
}

function volume_tabung(r, t) {
    return r**2 * Math.PI * t;
}

console.log("Sebelum mulai kalkulasi, pastikan untuk memasukkan jenis operasi yang sama persis seperti opsi yang ada yaa :D");
console.log("[opsi : kali, bagi, tambah, kurang, akar, kuadrat, luas_persegi, volume_kubus, volume_tabung]");
koneksi.question("Masukkan jenis kalkulasi : ", (jenis) => {
    if (jenis == "kali") {
        koneksi.question("Masukkan angka 1 : ", (x) => {
            koneksi.question("Masukkan angka 2 : ", (y) => {
                e = x;
                f = y;
                console.log("Hasil perkalian adalah ", kali(e, f));
            })
        })          
    }

    else if (jenis == "bagi") {
        koneksi.question("Masukkan angka 1 : ", (x1) => {
            koneksi.question("Masukkan angka 2 : ", (y1) => {
                g = x1;
                h = y1;
                console.log("Hasil pembagian adalah ", bagi(g, h));
            })
        })
    }

    else if (jenis == "tambah") {
        koneksi.question("Masukkan angka 1 : ", (x2) => {
            koneksi.question("Masukkan angka 2 : ", (y2) => {
                a = x2;
                b = y2;
                console.log("Hasil penjumlahan adalah ", tambah(a, b));
            })
        })
    }

    else if (jenis == "kurang") {
        koneksi.question("Masukkan angka 1 : ", (x3) => {
            koneksi.question("Masukkan angka 2 : ", (y3) => {
                c = x3;
                d = y3;
                console.log("Hasil pengurangan adalah ", kurang(c, d));
            })
        })
    }

    else if (jenis == "akar") {
        koneksi.question("Masukkan angka : ", (z) => {
            v = z;
            console.log("Hasil akar adalah ", akar(v));
        })
    }

    else if (jenis == "kuadrat") {
        koneksi.question("Masukkan angka : ", (z2) => {
            w = z2;
            console.log("Hasil Kuadrat adalah ", kuadrat(w));
        })
    }

            
    else if (jenis == "luas_persegi") {
        koneksi.question("Masukkan sisi : ", (sisi) => {
            s = sisi;
            console.log("Luas Persegi adalah ", luas_persegi(s));
        })
    }

    else if (jenis == "volume_kubus") {
        koneksi.question("Masukkan sisi kubus : ", (sisi1) => {
            s1 = sisi1;
            console.log("Volume Kubus adalah ", volume_kubus(s1));
        })
    }

    else if (jenis == "volume_tabung") {
        koneksi.question("Masukkan jari-jari : ", (jari) => {
            koneksi.question("Masukkan tinggi : ", (tinggi) => {
                r = jari;
                t = tinggi;
                console.log("Volume Tabung adalah ", volume_tabung(r, t))
            })
        })
    }

    else {
        console.log("Operasi yang kamu inginkan tidak ada :) ")
    } 
})

