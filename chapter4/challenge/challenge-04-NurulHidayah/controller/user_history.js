const {User_history, User_game} = require('../models');

createUhis = async (req,res) => {
    try {
        let {user_game_id, durasi, skor, waktu_permainan} = req.body;

        const user = await User_game.findOne({
            where: {
                id: user_game_id
            }
        });

        if(!user) {
            res.status(404).json({
                status: 'error',
                errors: 'tidak dapat menemukan user_game dengan id: ' + user_game_id
            })
            return;
        }

        let UserHisBaru = await User_history.create({
            user_game_id, 
            durasi, 
            skor, 
            waktu_permainan
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menambahkan data',
            data: UserHisBaru
        })

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getAll = async (req,res) => {
    try {
        let all = await User_history.findAll();

        res.status(200).json({
            status: 'success',
            message: 'berhasil menampilkan seluruh data',
            data: all
        });

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getDetail = async (req,res) => {
    try {
        const user_game_id = req.params.id;

        let detail = await User_history.findOne({
            where: {
                id: user_game_id
            },
            include: 'milik'
        });

        if(!detail) {
            res.status(404).json({
                status: 'error',
                message: 'data yang anda cari tidak tersedia',
                data: null
            })
        }

        res.status(200).json({
            status: 'success',
            message: 'berhasil menemukan data yang kamu cari',
            data: detail
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

updateUhis = async (req,res) => {
    try{
        const user_history_id = req.params.id;
        const {user_game_id, durasi, skor, waktu_permainan} = req.body;

        let query = {
            where: {
                id: user_history_id
            }
        };

        let update = await User_history.update({
            user_game_id, 
            durasi, 
            skor, 
            waktu_permainan
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'berhasil mengubah data',
            data: update
        });
    

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

deleteUhis = async (req,res) => {
    try{
        const user_history_id = req.params.id;

        let deleted = await User_history.destroy({
            where: {
                id: user_history_id
            }
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menghapus data',
            data: deleted
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}


module.exports = {
    createUhis,
    getAll,
    getDetail,
    updateUhis,
    deleteUhis
}