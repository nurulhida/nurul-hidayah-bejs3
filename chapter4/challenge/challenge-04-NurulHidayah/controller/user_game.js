const {User_game} = require('../models');

createUgame = async (req,res) => {
    try {
        let {username, password} = req.body;

        let UserGameBaru = await User_game.create({
            username,
            password
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menambahkan data',
            data: UserGameBaru
        })

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getAll = async (req,res) => {
    try {
        let all = await User_game.findAll();

        res.status(200).json({
            status: 'success',
            message: 'berhasil menampilkan seluruh data',
            data: all
        });

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getDetail = async (req,res) => {
    try {
        const user_game_id = req.params.id;

        let detail = await User_game.findOne({
            where: {
                id: user_game_id
            }
            ,
            include: ['biodata', 'history']
        });

        if(!detail) {
            res.status(404).json({
                status: 'error',
                message: 'data yang anda cari tidak tersedia',
                data: null
            })
        }

        res.status(200).json({
            status: 'success',
            message: 'berhasil menemukan data yang kamu cari',
            data: detail
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

updateUgame = async (req,res) => {
    try{
        const user_game_id = req.params.id;
        const {username, password} = req.body;

        let query = {
            where: {
                id: user_game_id
            }
        };

        let update = await User_game.update({
            username,
            password
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'berhasil mengubah data',
            data: update
        });
    

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

deleteUgame = async (req,res) => {
    try{
        const user_game_id = req.params.id;

        let deleted = await User_game.destroy({
            where: {
                id: user_game_id
            }
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menghapus data',
            data: deleted
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}


module.exports = {
    createUgame,
    getAll,
    getDetail,
    updateUgame,
    deleteUgame
}