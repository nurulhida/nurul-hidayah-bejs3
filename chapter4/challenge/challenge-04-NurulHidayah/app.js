const express = require('express');
const app = express();
const port = 7000;
const morgan = require('morgan');
const router = require('./routes');

app.use(morgan('dev'));
app.use(express.json());

app.use('/', router)



app.listen(port, () => {
    console.log('Aplikasi ini berjalan pada port', port)
})