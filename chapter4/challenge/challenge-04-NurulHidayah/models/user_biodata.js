'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_biodata.belongsTo(models.User_game, {foreignKey: 'user_game_id', as: 'owner'});
    }
  }
  User_biodata.init({
    user_game_id: DataTypes.INTEGER,
    nama_lengkap: DataTypes.STRING,
    email: DataTypes.STRING,
    negara: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_biodata',
  });
  return User_biodata;
};