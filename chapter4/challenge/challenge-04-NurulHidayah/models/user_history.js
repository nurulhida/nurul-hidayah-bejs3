'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_history.belongsTo(models.User_game, {foreignKey: 'user_game_id', as: 'milik'})
    }
  }
  User_history.init({
    user_game_id: DataTypes.INTEGER,
    durasi: DataTypes.STRING,
    skor: DataTypes.BIGINT,
    waktu_permainan: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_history',
  });
  return User_history;
};