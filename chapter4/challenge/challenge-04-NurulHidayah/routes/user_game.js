const express = require('express');
const router = express.Router();

const {createUgame, getAll, getDetail, updateUgame, deleteUgame} = require('../controller/user_game');

//CRUD
router.post('/', createUgame);
router.get('/', getAll);
router.get('/:id', getDetail);
router.put('/:id', updateUgame);
router.delete('/:id', deleteUgame);

module.exports = router;