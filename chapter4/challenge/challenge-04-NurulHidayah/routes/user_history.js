const express = require('express');
const router = express.Router();

const {createUhis, getAll, getDetail, updateUhis, deleteUhis} = require('../controller/user_history');

//CRUD
router.post('/', createUhis);
router.get('/', getAll);
router.get('/:id', getDetail);
router.put('/:id', updateUhis);
router.delete('/:id', deleteUhis);

module.exports = router;