const express = require('express');
const router = express.Router();

const user_game = require('./user_game');
const user_biodata = require('./user_biodata');
const user_history = require('./user_history');

router.get('/', (req,res) => {
    res.status(200).json({
        status: 'sukses',
        message: 'Selamat datang di game api '
    });
});

router.use('/user_game', user_game);
router.use('/user_biodata', user_biodata);
router.use('/user_history', user_history);

module.exports = router;