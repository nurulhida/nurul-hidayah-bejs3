const express = require('express');
const router = express.Router();

const {createUbio, getAll, getDetail, updateUbio, deleteUbio} = require('../controller/user_biodata');

//CRUD
router.post('/', createUbio);
router.get('/', getAll);
router.get('/:id', getDetail);
router.put('/:id', updateUbio);
router.delete('/:id', deleteUbio);

module.exports = router;