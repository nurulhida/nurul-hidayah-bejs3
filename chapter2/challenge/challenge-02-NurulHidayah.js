const readline = require('readline');

const masuk = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function terendah(angka) {
    minValue = Math.min.apply(null, angka);
    return minValue;
}

function tertinggi(angka) {
    maxValue = Math.max.apply(null, angka);
    return maxValue;
}

function urut(angka) {
    return angka.sort();
}

function data(nilai) {
    return new Promise(resolve => {
        masuk.question(nilai, a => {
            resolve(a); 
        })
    })
}

async function main() {
    let input;
    let angka = [];

    console.log("Program menghitung nilai siswa")

    while(input != 'q' || input != 'Q') {
        input = await data("Masukkan nilai : ")
        angka.push(parseInt(input));
        if (input == "q" || input == "Q") {
           masuk.close();
           angka.pop();

           let jumlah = 0;
           let panjang = angka.length;
           for (let i = 0; i<panjang; i++) {
            jumlah += angka[i];
           }
           let avg = jumlah / panjang;

           let lulus = angka.filter (function (x) {
               return x >= 75;
           })

           let tidakLulus = angka.filter (function (y) {
               return y < 75;
           })

           console.log("\nNilai Tertinggi : ", tertinggi(angka));
           console.log("Nilai Terendah : ", terendah(angka));
           console.log("Rata-Rata : ", avg);
           console.log("jumlah siswa lulus: ", lulus.length);
           console.log("Jumlah siswa yang tidak lulus: ", tidakLulus.length);
           console.log("Urutan nilai : ", urut(angka));
           break;
        }
    }  
}

main()
