const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function(req,file,callback) {
        callback(null, './public/upload');
    },

    filename: function(req,res,callback){
        const namaFile = Date.now() + '_' + path.extname(file.originalname);
        callback(null, namaFile);
    }
});

const upload = multer({
    storage: storage
    ,

    fileFilter: (req,file,callback) => {
        if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only png, jpg, and jpeg allowed to upload!'))
        }
    },

    onError: function(err, next) {
        console.log('error', err);
        next(err);
    }
});

module.exports = upload;