const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function(req,file,callback) {
        callback(null, './public/upload');
    },

    filename: function(req,res,callback){
        const namaFile = 'file' + Date.now() + '_' + path.extname(file.originalname);
        callback(null, namaFile);
    }
});

const upload = multer({
    storage: storage,

    fileFilter: (req,file,callback) => {
        if (file.mimetype == 'file/doc' || file.mimetype == 'file/docx' || file.mimetype == 'file/pdf') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only doc, docx, and pdf allowed to upload!'))
        }
    },

    onError: function(err, next) {
        console.log('error', err);
        next(err);
    }
});

module.exports = upload;