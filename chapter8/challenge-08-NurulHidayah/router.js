const express = require('express');
const router = express.Router();
const JWT = require('jsonwebtoken');
const {User_game} = require('./models')
const {
    JWT_SECRET_KEY
} = process.env

const user_game = require('./controllers/userGameController');
const user_biodata = require('./controllers/userBiodataController');
const user_history = require('./controllers/userHistoryController');

const auth = require('./controllers/authAdminController')
const midAuth = require('./middlewares/midAuth')
const ImageKit = require('./middlewares/index');
//home
router.get('/', (req,res) => {
    res.render('templates/welcome');
})

//web
router.get('/register', (req,res) => {
    res.render('auth/create_user_game');
})

router.get('/login', (req,res) => {
    res.render('auth/login');
})

router.get('/forgot-password', (req,res) => {
    res.render('auth/forgot');
})

router.get('/reset-password/:userId', async (req,res) => {
    try{
        const user_id = req.params.userId;
        const token = req.query.token;

        const user = await User_game.findOne({
            where: {
                id: user_id
            }
        })

        const secret = JWT_SECRET_KEY + user.password;
        const data = JWT.verify(token, secret);

        res.render('auth/reset', {token, user_id});
    }catch(err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        })
    }
})

//api
router.post('/user_game/login', user_game.login);
router.post('/user_game/forgot-password',user_game.forgot);
router.post('/user_game/reset-password/:userId', user_game.reset)
router.get('/user_game', user_game.getAll);
router.get('/user_game/:user_id', user_game.getDetail);
router.post('/user_game', user_game.createUgame);
router.put('/user_game/:user_id',  user_game.updateUgame);
router.delete('/user_game/:user_id', user_game.deleteUgame);

router.get('/user_biodata',  user_biodata.getAll);
router.get('/user_biodata/:id',  user_biodata.getDetail);
router.post('/user_biodata', user_biodata.createUbio);
router.put('/user_biodata/:id',  user_biodata.updateUbio);
router.delete('/user_biodata/:id', user_biodata.deleteUbio);

router.get('/user_history', user_history.getAll);
router.get('/user_history/:id', user_history.getDetail);
router.post('/user_history',  user_history.createUhis);
router.put('/user_history/:id', user_history.updateUhis);
router.delete('/user_history/:id',user_history.deleteUhis);

//upload file
const imageStorage = require('./middlewares/imageStorage');
const imageController = require('./controllers/imageController');
const videoStorage = require('./middlewares/videoStorage');
const videoController = require('./controllers/videoController');
const fileStorage = require('./middlewares/fileStorage');
const fileController = require('./controllers/fileController');
const { imagekit } = require('./helpers/kit');

//registrasi admin
router.post('/regist', auth.register)

//authentikasi untuk mendapat token untuk mengupload gambar/video/file
router.post('/auth/login-admin', auth.login)

//upload gambar
router.post('/upload/image', midAuth.login, midAuth.admin, imageStorage.single('image'), imageController.single);
router.post('upload/imagekit', ImageKit, imageController.imagekit)
router.post('/upload/video', midAuth.login, videoStorage.single('video', 5), videoController.multi);
router.post('/upload/file', midAuth.login, fileStorage.single('file', 15), fileController.multi)




module.exports = router;