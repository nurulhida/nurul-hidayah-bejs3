require('dotenv').config();
const nodemailer = require('nodemailer');
const {google} = require('googleapis');
const ejs = require('ejs');

const {
    OAUTH_REFRESH_TOKEN,
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET,
    OAUTH_REDIRECT_URI,
    SENDER_EMAIL
} = process.env;

const oauth2client = new google.auth.OAuth2(
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET,
    OAUTH_REDIRECT_URI
);

oauth2client.setCredentials({refresh_token : OAUTH_REFRESH_TOKEN});

module.exports = {
    sendEmail : async(to, subject, html) => {
        try {
            const access_token = await oauth2client.getAccessToken();

            const transportEmail = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    type: 'OAUTH2',
                    user: SENDER_EMAIL,
                    clientId: OAUTH_CLIENT_ID,
                    clientSecret: OAUTH_CLIENT_SECRET,
                    refreshToken: OAUTH_REFRESH_TOKEN,
                    accessToken: access_token
                }
            });

            const optionEmail = {
                to,
                subject,
                html
            };

            const response = await transportEmail.sendMail(optionEmail);

            return response;

        }catch(err) {
            return err;
        }
    },

    readHtml : (file_name, data) => {
        return new Promise((resolve, reject) => {
            const path = __dirname + '/../views/templates' + file_name;
            ejs.renderFile(path, data, (err, data) => {
                if (err) {
                    return reject(err);
                } else {
                    return reject(data)
                }
            })
        })
    }
}