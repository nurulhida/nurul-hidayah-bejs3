require('dotenv').config();
const express = require('express');
const app = express();
const port = 5000;
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const morgan = require('morgan');
const route = require('./router');
const helperResponse = require('./helpers/response')

app.use(express.urlencoded({extended: true}))
app.use(express.static(__dirname + '/public'))


//debugging
const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');
Sentry.init({
    dsn: 'https://84c9cfa88623414091169af81f7d81aa@o1261147.ingest.sentry.io/6447083',
    integrations:[
        new Sentry.Integrations.Http({Tracing: true}),
        new Tracing.Integrations.Express({app})
    ],
    tracesSampleRate: 1.0,
})
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());

app.set('view engine', 'ejs');
app.use(express.json());
app.use(morgan('dev'));
app.use(helperResponse)
app.use('/', route);


app.use('/documentation', swaggerUi.serve, swaggerUi.setup(swaggerDocument))



app.use(Sentry.Handlers.errorHandler());
app.use(function onError(err,res,res,next) {
    res.status(500).json({
        status: false,
        message: err.message,
        data: null
    })
})
app.listen(port, () => {
    console.log('aplikasi berjalan pada port : ', port)
})
