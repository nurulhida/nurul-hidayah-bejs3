const {File} = require('../models');

module.exports = {
    multi: async (req,res) => {
        try{
            const fileUrl = req.protocol + '://' + req.get('host') + '/file/' + req.file.filename;

            const file = await File.create({
                title: req.file.filename,
                url: fileUrl
            });

            res.status(200).json({
                status: true,
                message: 'success',
                data: file
            });

        }catch(err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    }
}