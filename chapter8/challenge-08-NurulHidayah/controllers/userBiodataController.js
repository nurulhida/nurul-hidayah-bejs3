const {User_biodata, User_game} = require('../models');
const validator = require('fastest-validator');
const v = new validator();

createUbio = async (req,res) => {
    try {
        let {user_game_id, nama_lengkap, negara} = req.body;

        const schema = {
            user_game_id: {type: 'number', integer: true},
            nama_lengkap: {type: 'string'},
            negara: {type: 'string', optional: true}
        }

        const validate = v.validate(req.body, schema);
        if(validate.length) {
            res.respondBadReq(validate)
            return;
        };

        const ada = await User_biodata.findOne({
            where: {
                user_game_id,
                nama_lengkap,
                negara
            }
        });

        if (ada) {
            res.status(400).json({
                status: 'error',
                errors: 'satu user hanya bisa mempunyai satu biodata, jika ingin merubah pilih opsi update yaa^^'
            });
            return;
        }

        const user = await User_game.findOne({
            where: {
                id: user_game_id
            }
        });

        if(!user) {
            res.status(404).json({
                status: 'error',
                errors: 'tidak dapat menemukan user_game dengan id: ' + user_game_id
            })
            return;
        }


        let UserBioBaru = await User_biodata.create({
            user_game_id, 
            nama_lengkap,  
            negara
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menambahkan data',
            data: UserBioBaru
        })

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getAll = async (req,res) => {
    try {
        let all = await User_biodata.findAll();

        res.status(200).json({
            status: 'success',
            message: 'berhasil menampilkan seluruh data',
            data: all
        });

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getDetail = async (req,res) => {
    try {
        const user_biodata_id = req.params.id;

        let detail = await User_biodata.findOne({
            where: {
                id: user_biodata_id
            },
            include: 'owner'
        });

        if(!detail) {
            res.status(404).json({
                status: 'error',
                message: 'data yang anda cari tidak tersedia',
                data: null
            })
        }

        res.status(200).json({
            status: 'success',
            message: 'berhasil menemukan data yang kamu cari',
            data: detail
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

updateUbio = async (req,res) => {
    try{
        const user_biodata_id = req.params.id;
        const {user_game_id, nama_lengkap, negara} = req.body;

        const schema = {
            user_game_id: {type: 'number', integer: true, optional: true},
            nama_lengkap: {type: 'string', optional: true},
            negara: {type: 'string', optional: true}
        }

        const validate = v.validate(req.body, schema);
        if(validate.length) {
            res.respondBadReq(validate)
            return;
        };

        let query = {
            where: {
                id: user_biodata_id
            }
        };

        let update = await User_biodata.update({
            user_game_id: req.body.user_game_id, 
            nama_lengkap: req.body.nama_lengkap, 
            negara: req.body.negara
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'berhasil mengubah data',
            data: update
        });
    

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

deleteUbio = async (req,res) => {
    try{
        const user_biodata_id = req.params.id;

        let deleted = await User_biodata.destroy({
            where: {
                id: user_biodata_id
            }
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menghapus data',
            data: deleted
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}


module.exports = {
    createUbio,
    getAll,
    getDetail,
    updateUbio,
    deleteUbio
}