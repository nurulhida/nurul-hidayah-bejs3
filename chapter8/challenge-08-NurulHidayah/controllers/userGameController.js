require('dotenv').config();
const {User_game} = require('../models');
const validator = require('fastest-validator');
const v = new validator();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const helpers = require('../helpers/mailerHelper');
const help = require('nodemon/lib/help');
const {
    JWT_SECRET_KEY
} = process.env;


login = async (req,res) => {
    try{
        const {email, password} = req.body;

        const user = await User_game.findOne({
            where: {
                email: email
            }
        });

        if(!user) {
            return res.status(400).json({
                status: false,
                message: 'user didnt exist!',
                data: null
            });
        }

        const isCorrect = await bcrypt.compare(password, user.password);

        if (!isCorrect) {
            return res.status(400).json({
                status: false,
                message: 'wrong password!',
                data: null
            })
        }

        const data = {
            id: user.id,
            username: user.username,
            email: user.email
        }

        const token = await jwt.sign(data, JWT_SECRET_KEY);

        return res.json({
            status: true,
            message: 'login successfully!',
            data: {
                ...data,
                token
            }
        })
    }catch(err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        })
    }
}

forgot = async (req,res) => {
    try{
        const {username, email} = req.body;

        const user = await User_game.findOne({
            where: {
                email: email
            }
        });

        if (user) {
            const data = {
                id: user.id,
                username: user.username,
                email: user.email
            };

            const secret = JWT_SECRET_KEY + user.password;

            const token = await jwt.sign(data, secret, {expiresIn: '15m'});

            const link = `${req.protocol}://${req.get('host')}/reset-password/${user.id}?token=${token}`;

            const html = `
            <p>hi ${user.username}, klik <a href="${link}" target="_blank"> disini </a> untuk mengatur ulang password</p>
            `;

            helpers.sendEmail(user.email, 'reset password', html);
        }

        return res.status(200).json({
            status: true,
            message: 'silahkan cek email untuk mendapatkan link reset password',
            data: null
        })

    }catch(err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        });
    }
}

reset = async (req,res) => {
    try {
        const token = req.query.token;

        const user_id = req.params.userId;

        const {password_baru, password_baru_confirm} = req.body;

        if(password_baru != password_baru_confirm) {
            return res.status(500).json({
                status: false,
                message: 'konfirmasi password harus sama dengan password yang dibuat!',
                data: null
            })
        }

        const user = await User_game.findOne({
            where: {
                id: user_id
            }
        });

        const secret = JWT_SECRET_KEY + user.password;
        const data = await jwt.verify(token, secret);

        const hashPassword = await bcrypt.hash(password_baru, 10);

        let search = {
            where: {
                email: data.email
            }
        }

        let update_password = await User_game.update({
            passsword: hashPassword
        }, search);

        return res.status(200).json({
            status: true,
            message: 'Berhasil memperbarui password',
            data: update_password
        })
    }catch(err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        });
    }
}

createUgame = async (req,res) => {
    try {
        let {username, email, password} = req.body;

        const schema = {
            username: {type: 'string', min: 3, max: 255},
            email: {type: 'string'},
            password: {type: 'string', min: 8}
        }

        const validate = v.validate(req.body, schema)
        if(validate.length) {
            res.respondBadReq(validate)
            return;
        };

        if (!username || !email|| !password) {
            res.respondBadReq('name, email, and password is required!');
            return;
        }

        const hashPassword = await bcrypt.hash(password, 10)
        const UserGameBaru = await User_game.create({
            username,
            email,
            password : hashPassword
        });

        const link = `${req.protocol}://${req.get('host')}/login`;

        const html = `
            <h3>hi ${UserGameBaru.username}, Selamat akun kamu berhasil dibuat!</h3>
            <p>untuk menggunakannya, login terlebih dahulu <a href="${link}" target="_blank"> disini </a> </p>
        `;

        helpers.sendEmail(UserGameBaru.email, 'New Account', html);

        res.status(200).json({
            status: 'success',
            message: 'berhasil membuat akun, pesan akan otomatis dikirim pada email!',
            data: UserGameBaru
        })

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getAll = async (req,res) => {
    try {
        let all = await User_game.findAll();

        res.status(200).json({
            status: 'success',
            message: 'berhasil menampilkan seluruh data',
            data: all
        });

    }
    catch(err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

getDetail = async (req,res) => {
    try {
        const user_game_id = req.params.id;

        let detail = await User_game.findOne({
            where: {
                id: user_game_id
            }
            ,
            include: ['biodata', 'history']
        });

        if(!detail) {
            res.status(404).json({
                status: 'error',
                message: 'data yang anda cari tidak tersedia',
                data: null
            })
        }

        res.status(200).json({
            status: 'success',
            message: 'berhasil menemukan data yang dimaksud',
            data: detail
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

updateUgame = async (req,res) => {
    try{
        const user_game_id = req.params.id;
        const {username, email} = req.body;

        const schema = {
            username: 'string|min:3|max:255|optional',
            email: 'email|optional',
        };

        const validate = v.validate(req.body, schema)
        if(validate.length) {
            res.respondBadReq(validate)
            return;
        };

        let query = {
            where: {
                id: user_game_id
            }
        };

        let update = await User_game.update({
            username,
            email
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'berhasil mengubah data',
            data: update
        });
    

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}

deleteUgame = async (req,res) => {
    try{
        const user_game_id = req.params.id;

        let deleted = await User_game.destroy({
            where: {
                id: user_game_id
            }
        });

        res.status(200).json({
            status: 'success',
            message: 'berhasil menghapus data',
            data: deleted
        })

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
}


module.exports = {
    createUgame,
    getAll,
    getDetail,
    updateUgame,
    deleteUgame,
    login,
    forgot,
    reset
}