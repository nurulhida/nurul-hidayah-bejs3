const {Image} = require('../models');
const {imagekit} = require('../helpers/kit')

module.exports = {
    single: (req,res) => {
        try{
            res.json({
                ...req.file,
                file_url: 'http://localhost:5000/image/' + req.file.filename
            })
        }catch(err){
            res.json(err)
        }
    },

    multi: (req,res) => {
        try{
            res.json(req.files)
        }catch(err) {
            res.json(err)
        }
    },

    imagekit: async (req,res) => {
        try{
            const file = req.file.buffer.toString("base64");
            const namafile = Date.now() + '_' + Math.round(Math.random() * 1E9) + req.file.originalname

            const upload = await imagekit.upload({
                file: file,
                fileName: namafile
            })

            res.status(200).json({
                status: true,
                message: 'success',
                data: upload
            })
        }catch(err){
            return res.status(500).json({
                status: false,
                message: err.message,
                dat: null
            });
        }
        // res.send(req.file);
    }

    
}