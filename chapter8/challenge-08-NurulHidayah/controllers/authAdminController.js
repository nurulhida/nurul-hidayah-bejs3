const {Admin} = require('../models');
const bcrypt = require('bcrypt');
const Validator = require('fastest-validator');
const v = new Validator();
const jwt = require('jsonwebtoken');

module.exports = {
    register: async (req, res) => {
        try {
            const { username, email, password } = req.body;
            const isExist = await Admin.findOne({ 
                where: { 
                    email: email 
                } 
            });

            if (isExist) {
                return res.status(400).json({
                    status: false,
                    message: 'user already exist!',
                    data: null
                });
            }

            const hashPassword = await bcrypt.hash(password, 10);

            const newUser = await Admin.create({
                username: username,
                email: email,
                password: hashPassword
            });

            res.status(201).json({
                status: true,
                message: 'user registered',
                data: {
                    id: newUser.id,
                    username: newUser.username,
                    email: newUser.email,
                    updatedAt: newUser.updatedAt,
                    createdAt: newUser.createdAt
                }
            });

        } catch (err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    },
    
    login: async (req,res) => {
        try{
            const schema = {
               username: 'string|required',
               password: 'string|required' 
            }

            const validate = v.validate(req.body, schema);
            if(validate.length) {
                return res.status(400).json({
                    status: false,
                    message: 'bad request!',
                    data: validate
                });
            }

            const user_game = await Admin.findOne({
                where: {
                    username: req.body.username
                }
            });

            if(!user_game) {
                return res.status(400).json({
                    status: false,
                    message: 'user not found!',
                    data: null
                });
            }

            const data = {
                username : user_game.username,
                password : user_game.password,
                role: "admin"
            }

            const secretKey = process.env.JWT_SECRET_KEY;
            const token = jwt.sign(data, secretKey);

            res.status(200).json({
                status: true,
                message: 'ok',
                data: {
                    ...data,
                    token
                }
            });

        } catch(err){
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    }
}