const express = require('express');
const app = express();
const port = 5000;
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const morgan = require('morgan');
const route = require('./router');
const helperResponse = require('./helpers/response')

app.set('view engine', 'ejs');
app.use(express.json());
app.use(morgan('dev'));
app.use(helperResponse)
app.use(route);

app.use('/documentation', swaggerUi.serve, swaggerUi.setup(swaggerDocument))



app.listen(port, () => {
    console.log('aplikasi berjalan pada port : ', port)
})
