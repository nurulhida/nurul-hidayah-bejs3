const express = require('express');
const router = express.Router();

const user_game = require('./controllers/userGameController');
const user_biodata = require('./controllers/userBiodataController');
const user_history = require('./controllers/userHistoryController');

router.get('/user_game', user_game.getAll);
router.get('/user_game/:id', user_game.getDetail);
router.post('/user_game', user_game.createUgame);
router.put('/user_game/:id', user_game.updateUgame);
router.delete('/user_game/:id', user_game.deleteUgame);

router.get('/user_biodata', user_biodata.getAll);
router.get('/user_biodata/:id', user_biodata.getDetail);
router.post('/user_biodata', user_biodata.createUbio);
router.put('/user_biodata/:id', user_biodata.updateUbio);
router.delete('/user_biodata/:id', user_biodata.deleteUbio);

router.get('/user_history', user_history.getAll);
router.get('/user_history/:id', user_history.getDetail);
router.post('/user_history', user_history.createUhis);
router.put('/user_history/:id', user_history.updateUhis);
router.delete('/user_history/:id', user_history.deleteUhis);


module.exports = router;